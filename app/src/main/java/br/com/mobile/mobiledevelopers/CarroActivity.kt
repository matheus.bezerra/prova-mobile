package br.com.mobile.mobiledevelopers

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import br.com.mobile.mobiledevelopers.databinding.ActivityCarroBinding
import com.squareup.picasso.Picasso

class CarroActivity : DebugActivity() {
    private val binding by lazy {
        ActivityCarroBinding.inflate(layoutInflater)
    }

    var carro: Carro? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        carro = intent.getSerializableExtra("carro") as Carro

        setSupportActionBar(binding.toolbarInclude.toolbar)

        supportActionBar?.title = carro?.modelo

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.modeloCarro.text = carro?.modelo
        binding.anoCarro.text = carro?.ano
        binding.fabricanteCarro.text = carro?.fabricante
        binding.precoCarro.text = carro?.preco
        Picasso.with(this).load(carro?.foto).fit().into(binding.fotoCarro,
            object: com.squareup.picasso.Callback{
                override fun onSuccess() {}
                override fun onError() { }
            })

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_carro, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item?.itemId
        if  (id == R.id.action_remover) {
            AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("Deseja excluir o carro?")
                .setPositiveButton("Sim") {
                        dialog, which ->
                    dialog.dismiss()
                    taskExcluir()
                }.setNegativeButton("Não") {
                        dialog, which -> dialog.dismiss()
                }.create().show()
        }
        else if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun taskExcluir() {
        if (this.carro != null && this.carro is Carro) {
            Thread {
                CarroService.delete(this.carro as Carro)
                runOnUiThread {
                    finish()
                }
            }.start()
        }
    }
}