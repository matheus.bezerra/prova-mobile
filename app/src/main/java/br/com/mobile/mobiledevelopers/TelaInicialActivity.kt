package br.com.mobile.mobiledevelopers

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.mobile.mobiledevelopers.databinding.ActivityTelaInicialBinding
import com.google.android.material.navigation.NavigationView

class TelaInicialActivity : DebugActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val context: Context get() = this
    private var carros = listOf<Carro>()
    private var REQUEST_CADASTRO = 1
    private var REQUEST_REMOVE= 2

    private val binding by lazy {
        ActivityTelaInicialBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarInclude.toolbar)
        supportActionBar?.title = "Carros"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        configuraMenuLateral()

        binding.recyclerCarros?.layoutManager = LinearLayoutManager(context)
        binding.recyclerCarros?.itemAnimator = DefaultItemAnimator()
        binding.recyclerCarros?.setHasFixedSize(true)
    }

    override fun onResume() {
        super.onResume()
        taskCarros()
    }

    fun taskCarros() {
        Thread {
            this.carros = CarroService.getCarros(context)
            runOnUiThread {

                binding.recyclerCarros?.adapter =
                    CarroAdapter(carros) { onClickCarro(it) }

                fun enviaNotificacao(carro: Carro) {

                    val intent = Intent(this, CarroActivity::class.java)

                    intent.putExtra("carro", carro)

                    NotificationUtil.create(1, intent, "Developers", "Você tem nova mensagem na ${carro.modelo}")
                }
            }
        }.start()
    }

    fun onClickCarro(carro: Carro) {
        Toast.makeText(context, "Clicou carro ${carro.modelo}", Toast.LENGTH_SHORT)
            .show()
        val intent = Intent(context, CarroActivity::class.java)
        intent.putExtra("carro", carro)
        startActivityForResult(intent, REQUEST_REMOVE)
    }

    private fun configuraMenuLateral() {
        var toogle = ActionBarDrawerToggle(
            this,
            binding.layoutMenuLateral,
            binding.toolbarInclude.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close)
        binding.layoutMenuLateral.addDrawerListener(toogle)
        toogle.syncState()
        binding.menuLateral.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_carros -> {
                Toast.makeText(this, "Clicou Carros", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_mensagens -> {
                Toast.makeText(this, "Clicou Mensagens", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_forum -> {
                Toast.makeText(this, "Clicou Forum", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_localizacao -> {
                Toast.makeText(this, "Clicou Localização", Toast.LENGTH_SHORT).show()
            }
            R.id.nav_config -> {
                Toast.makeText(this, "Clicou Config", Toast.LENGTH_SHORT).show()
            }
        }
        binding.layoutMenuLateral.closeDrawer(GravityCompat.START)
        return true
    }

    fun cliqueSair() {
        val returnIntent = Intent();
        returnIntent.putExtra("result","Saída do BrewerApp");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
    // infla o menu com os botões da ActionBar
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
// id do item clicado
        val id = item?.itemId
        if (id == R.id.action_buscar) {
            Toast.makeText(context, "Botão de buscar",
                Toast.LENGTH_LONG).show()

        } else if (id == R.id.action_atualizar) {
            Toast.makeText(context, "Botão de atualizar",
                Toast.LENGTH_LONG).show()

        }else if (id == R.id.action_adicionar) {
            val intent = Intent(context, CarroCadastroActivity::class.java)
            startActivityForResult(intent, REQUEST_CADASTRO)
        }        else if (id == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CADASTRO || requestCode == REQUEST_REMOVE ) {
            // atualizar lista de carros
            taskCarros()
        }
    }
}