package br.com.mobile.mobiledevelopers

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.net.URL

object CarroService {

    val host = "http://172.18.37.16:5000"
    val TAG = "MobileDevelopers"

    fun getCarros (context: Context): List<Carro> {
        var carros = ArrayList<Carro>()
        if (AndroidUtils.isInternetDisponivel(context)) {
            val url = "$host/carros"
            val json = HttpHelper.get(url)
            carros = parserJson(json)
// salvar offline
            for (d in carros) {
                saveOffline(d)
            }
            return carros
        } else {
            val dao = DatabaseManager.getCarroDAO()
            val carros = dao.findAll()
            return carros
        }
    }

    fun saveOffline(carro: Carro) : Boolean {
        val dao = DatabaseManager.getCarroDAO()
        if (! existeCarro(carro)) {
            dao.insert(carro)
        }
        return true
    }

    fun existeCarro(carro: Carro): Boolean {
        val dao = DatabaseManager.getCarroDAO()
        return dao.getById(carro.id) != null

    }

    fun save(carro: Carro): Response {
        val json = HttpHelper.post("$host/carros", carro.toJson())
        return parserJson<Response>(json)
    }

    fun delete(carro: Carro): Response {
        if (AndroidUtils.isInternetDisponivel(LMSApplication.getInstance().applicationContext)) {
            val url = "$host/carros/${carro.id}"
            val json = HttpHelper.delete(url)
            return parserJson(json)
        } else {
            val dao = DatabaseManager.getCarroDAO()
            dao.delete(carro)
            return Response(status = "OK", msg = "Dados salvos localmente")
        }
    }

    inline fun <reified T> parserJson(json: String): T {
        val type = object : TypeToken<T>(){}.type
        return Gson().fromJson<T>(json, type)
    }
}