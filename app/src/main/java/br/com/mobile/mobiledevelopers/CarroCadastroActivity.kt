package br.com.mobile.mobiledevelopers

import android.os.Bundle
import br.com.mobile.mobiledevelopers.databinding.ActivityCadastroCarroBinding
import kotlinx.android.synthetic.main.activity_cadastro_carro.*

class CarroCadastroActivity : DebugActivity() {

    private val binding by lazy {
        ActivityCadastroCarroBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setTitle("Novo Carro")

        binding.salvarCarro.setOnClickListener {
            val carro = Carro()
            carro.modelo = modeloCarro.text.toString()
            carro.ano = anoCarro.text.toString()
            carro.foto = fotoCarro.text.toString()
            carro.fabricante = fabricanteCarro.text.toString()
            carro.preco = precoCarro.text.toString()
            taskAtualizar(carro)
        }
    }
    private fun taskAtualizar(carro: Carro) {
        Thread {
            CarroService.save(carro)
            runOnUiThread {
                finish()
            }
        }.start()
    }
}