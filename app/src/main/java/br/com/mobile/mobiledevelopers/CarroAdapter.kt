package br.com.mobile.mobiledevelopers

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class CarroAdapter (
    val carros: List<Carro>,
    val onClick: (Carro) -> Unit):
    RecyclerView.Adapter<CarroAdapter.CarrosViewHolder>() {

    // ViewHolder com os elementos da tela
    class CarrosViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val cardNome: TextView
        val cardImg : ImageView
        var cardProgress: ProgressBar
        init {
            cardNome = view.findViewById<TextView>(R.id.cardNome)
            cardImg = view.findViewById<ImageView>(R.id.cardImg)
            cardProgress = view.findViewById<ProgressBar>(R.id.cardProgress)
        }
    }
    // Quantidade de disciplinas na lista
    override fun getItemCount() = this.carros.size

    // inflar layout do adapter
    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int): CarrosViewHolder {

// infla view no adapter
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_carro, parent, false)

// retornar ViewHolder
        val holder = CarrosViewHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: CarrosViewHolder, position: Int) {
        val context = holder.itemView.context

// recuperar objeto disciplina
        val carro = carros[position]

// atualizar dados de disciplina
        holder.cardNome.text = carro.modelo
        holder.cardProgress.visibility = View.VISIBLE

// download da imagem
        Picasso.with(context).load(carro.foto).fit().into(holder.cardImg,
            object: com.squareup.picasso.Callback{
                override fun onSuccess() {
                    holder.cardProgress.visibility = View.GONE
                }
                override fun onError() {
                    holder.cardProgress.visibility = View.GONE
                }
            })

// adiciona evento de clique
        holder.itemView.setOnClickListener {onClick(carro)}
    }
}