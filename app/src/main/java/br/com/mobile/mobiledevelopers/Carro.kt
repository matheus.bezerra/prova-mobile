package br.com.mobile.mobiledevelopers

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.GsonBuilder
import java.io.Serializable

@Entity(tableName = "carro")
class Carro : Serializable {

    @PrimaryKey
    var id: Long = 0
    var modelo = ""
    var ano = ""
    var foto = ""
    var fabricante = ""
    var preco = ""

    override fun toString(): String {
        return "Carro(modelo='$modelo')"
    }

    fun toJson(): String {
        return GsonBuilder().create().toJson(this)
    }
}