package br.com.mobile.mobiledevelopers

import androidx.room.Room

object DatabaseManager {
    // singleton
    private var dbInstance: CarrosDatabase
    init {
        val appContext = LMSApplication.getInstance().applicationContext
        dbInstance = Room.databaseBuilder(
            appContext, // contexto global
            CarrosDatabase::class.java, // Referência da classe do banco
            "lms.sqlite" // nome do arquivo do banco

        ).build()
    }
    fun getCarroDAO(): CarroDAO {
        return dbInstance.carroDAO()
    }
}