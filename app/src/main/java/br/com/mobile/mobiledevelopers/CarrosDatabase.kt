package br.com.mobile.mobiledevelopers

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(Carro::class), version = 1)
abstract class CarrosDatabase: RoomDatabase() {
    abstract fun carroDAO(): CarroDAO
}