package br.com.mobile.mobiledevelopers


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import br.com.mobile.mobiledevelopers.databinding.LoginBinding
import kotlinx.android.synthetic.main.login.view.*


class MainActivity : DebugActivity() {

    private val context: Context get() = this

    private val binding by lazy{
        LoginBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.botaoLogin.setOnClickListener { onClickLogin() }

        var lembrar = Prefs.getBoolean("lembrar")
        if (lembrar) {
            binding.checkLembrar.isChecked = lembrar
        }
    }

    fun onClickLogin() {

        val valorUsuario = binding.campoUsuario.text.toString()
        val valorSenha = binding.campoSenha.text.toString()

        // armazenar valor do checkbox
        Prefs.setBoolean("lembrar", binding.checkLembrar.isChecked)
        // verificar se é para pembrar nome e senha
        if (binding.checkLembrar.isChecked) {
            Prefs.setString("lembrarNome", valorUsuario)
            Prefs.setString("lembrarSenha", valorSenha)
        } else {
            Prefs.setString("lembrarNome", "")
            Prefs.setString("lembrarSenha", "")
        }

        // criar intent
        val intent = Intent(context, TelaInicialActivity::class.java)
        // colocar parâmetros (opcional)


        // fazer a chamada
        //startActivity(intent)

        // fazer a chamada esperando resultado
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            val result = data?.getStringExtra("result")
            Toast.makeText(context, "$result", Toast.LENGTH_LONG).show()
        }
    }
    
}